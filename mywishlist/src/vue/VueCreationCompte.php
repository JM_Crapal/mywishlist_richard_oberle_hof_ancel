<?php
	namespace mywishlist\vue;

	class VueCreationCompte{
		private $choixFonctionnalite;

		public function __construct($choix){
			$this->choixFonctionnalite = $choix;
		}

		private function head(){
			$app = \Slim\Slim::getInstance();
      		$rootUri = $app->request->getRootUri();
      		$urlCss = $rootUri."/src/css/CssMain.css";
			$head = "<head><meta charset='UTF-8'><title>Wish List</title><link rel=\"stylesheet\" href=\"$urlCss\"></head> ";
			return $head;
		}

		#affichage menu
		public static function menu(){
			$app = \Slim\Slim::getInstance();
			$urlAccueil = $app->urlFor('acceuilAff');
			$urlCreateur = $app->urlFor('creerCreateur');
			$urlListes = $app->urlFor('listingListe');
			$urlItems = $app->urlFor('listingItem');
			$urlConnexion = $app->urlFor('connection');
			$urlDeconnexion = $app->urlFor('deconnection');
			$menu = "";
			if($_SESSION['pouvoir'] == 100){
							$menu = <<<END
			<nav class="Menu" style="z-index:3;width:250px" id="Sidebar">
				<a href="$urlAccueil" class="ButtonConnexion">Accueil</a>
				<a href="$urlCreateur" class="ButtonConnexion">Créer un compte</a>
				<a href="$urlConnexion" class="ButtonConnexion>Se connecter</a>
				<a href="$urlListes" class="ButtonMenu">Afficher listes</a>
				<a href="$urlItems" class="ButtonMenu">Afficher items</a>
			</nav>
END;
			}else{
				<<<END
			<nav class="Menu" style="z-index:3;width:250px" id="Sidebar">
				<a href="$urlAccueil" class="ButtonConnexion">Accueil</a>
				<a href="$urlCreateur" class="ButtonConnexion">Créer un compte</a>
				<a href="$urlDeconnexion" class="ButtonConnexion>Deconnexion</a>
				<a href="$urlListes" class="ButtonMenu">Afficher listes</a>
				<a href="$urlItems" class="ButtonMenu">Afficher items</a>
			</nav>
END;
			}
			return $menu;
		}

		public function render(){
			if($this->choixFonctionnalite == 1){
				$app = \Slim\Slim::getInstance();
				$urlConfirmation = $app->urlFor('confirmerCreateur');
				$head = $this->head();
				$menu = $this->menu();
				$html = <<<END
				<!Doctype html>
				<html>
					$head
				<body id=CreationCompte>
				<header>Creation compte createur</header>
				$menu
				<form id="formulaireCreationCompteCreateur" method="post" action="$urlConfirmation"

				<div class="id"></br>Identifiant : <br><input type="text" id="idCompteCreateur" name="identifiant" placeholder="identifiant ici" required></br></br></div>
				<div class="mdp">Mot de passe : <br><input type="password" id="mdpCompteCreateur" name="motDePasse" placeholder="mot de passe ici" required></br></br></div>
				<div class="confirmdp">Confirmation Mot de passe : <br><input type="password" id="confirmationMdp" name="confirmeMdp" placeholder="confirmer le mot de passe" required></br></br></div>
				<button type=submit
	 			name="valider_inc"
	 			value="valid_f1">
	 			valider
				</button>
				</form>
				</body></html>
END;

				if( isset( $_SESSION['erreur'])){
					echo $_SESSION['erreur'];
					echo "</br></br>";
					unset($_SESSION['erreur']);
				}
				echo $html;
				exit();
			}

			if($this->choixFonctionnalite == 2){
				$app = \Slim\Slim::getInstance();
				$urlConfirmation = $app->urlFor('confirmerConnection');
				$head = $this->head();
				$html = <<<END
				<!Doctype html>
				<html>
					$head
				<body>
				<header>Connection compte</header>
				<form id="formulaireCreationCompteCreateur" method="post" action="$urlConfirmation"

				<div class="id"></br>Identifiant : <input type="text" id="idCompteCreateur" name="identifiant" placeholder="identifiant ici" required></br></br></div>
				<div class="mdp">Mot de passe : <input type="password" id="mdpCompteCreateur" name="motDePasse" placeholder="mot de passe ici" required></br></br></div>
				<button type=submit
	 			name="valider_inc"
	 			value="valid_f1">
	 			valider
				</button>
				</form>
				</body></html>
END;

				if( isset( $_SESSION['erreur'])){
					echo $_SESSION['erreur'];
					echo "</br></br>";
					unset($_SESSION['erreur']);
				}
				echo $html;
				exit();
			}

		}

	}
