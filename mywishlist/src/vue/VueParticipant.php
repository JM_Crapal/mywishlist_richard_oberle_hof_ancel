<?php
	namespace mywishlist\vue;

	class VueParticipant{
		public $contenu;
		public $select;

		const LISTE_LISTES =1;
		const LISTE_LISTE_ITEMS = 2;
		const LISTE_ITEM = 3;
		const LISTE_ITEMS = 4;
		const CREATION_ITEM = 5;
		const LISTE_CREER = 6;
		const AFFICHAGE_LISTE_USER = 7;
		const FORMULAIRE_AJOUT_ITEM = 8;
		const ITEM_AJOUTER = 9;
		const RESERVE_ITEM = 10;
		const RESERVE_ITEM_deja_res = 11;
		const RESERVE_ITEM_NomParticipant = 12;

		#Constructeur
		public function __construct($tabObjet, $select){
			$this->contenu = $tabObjet;
			$this->select = $select;
		}

		#Affichage de toutes les listes (leur nom)
		private function affichageListes(){
			$app = \Slim\Slim::getInstance();
			$res = "<section> <br/>";
			foreach ($this->contenu as $key => $value) {
				$itemUrl = $app->urlFor('listeParId', [ 'id' => $value["no"] ]);
				$url = "/".$itemUrl;
				$res.=  "<a href=$itemUrl".">".$value["titre"]."</a>"."<h1 id='noms'></h1><p id=descListe>";
				$res.=  $value["description"]."<br/>";
				$res.=  $value["expiration"]."<br/></p><br>";
			}
			$itemUrl = $app->urlFor('creerListeGet');
			$url = $itemUrl;
			$res .= "<a href=$url>Creer une nouvelle liste</a>";
			$res = $res . "</section>";
			return $res;
		}

		#Une liste en particulier (le contenu de cette liste)
		private function affichageListe(){
			$res = "<p id= 'instruction'>Cliquez sur le nom d'un item pour en voir le détail et pouvoir le réserver.</p>";
			$res .= "<section> <br/><table><th>Article</th><th>Description</th><th>Image</th><th>Tarif (€)</th>";
			$app = \Slim\Slim::getInstance();
			$rootUri = $app->request->getRootUri();
			foreach($this->contenu as $k=>$v){
				$itemUrl = $app->urlFor('itemParId', [ 'id' => $v["id"] ]);
				$url = $rootUri."/".$itemUrl;
				$urlImage = $rootUri."/src/images/".$v["img"];
				$res.="<tr><td id='lienTD'><a id='lienVersItem' href=$itemUrl>".$v["nom"]."</a></td>";
				$res.="<td>".$v["descr"]."</td>";
				$res.="<td><img id='imageItemListe' src=$urlImage></td>";
				$res.="<td id='tarifTD'>".$v["tarif"]."</td></tr><br>";
			}
			$res = $res . "</table></section>";
			return $res;
		}

		#Creation d'une liste
    	public function affichageCreationListe(){
    		$app = \Slim\Slim::getInstance();
    		$listeUrl = $app->urlFor('listingListe');
    		$url = "/".$listeUrl;
			return "<form method=POST >
			<input type=text name=titre placeholder=Nom de la liste></input>
			<textarea type=text name=description placeholder=Description de votre liste></textarea>
			<input type=text name=dateExpiration placeholder=Date limite (YYYY/MM/JJ)></input>
			<button type=submit>Creer la liste</button>
			<a href=$url>Retour</a>
			</form>";
		}

		#Affichage liste creer ainsi que l'url generer
		public function listeCreer(){
			$contenu = "<div class=\"listeCreer\">Votre liste a été créée</br>Si vous voulez la modifier ou bien ajouter des objets a cette liste ou bien la partagez veuillez utiliser l'url si dessous</br>";
			$contenu = $contenu.$_SESSION['erreur']."</div>";
			return $contenu;
		}

		#Un item particulier
		public function affichageItem(){
			$app = \Slim\Slim::getInstance();
			$rootUri = $app->request->getRootUri();
			$urlImage = $rootUri."/src/images/".$this->contenu["img"];
			$res = "<section id ='item'> <br/>";
			$res.="Nom: ".$this->contenu["nom"]."<br/>";
			$res.="Description: ".$this->contenu["descr"]."<br/>";
			$res.="<img id='imageItem' src='$urlImage'>"."<br/>";
			$res.="Prix: ".$this->contenu["tarif"]."€<br/>";
			$urlRes = $app->urlFor('participantRes',['id'=>$this->contenu['id']]);
			if(isset($this->contenu['reserve']) && $this->contenu['reserve']=="false"){
				$res.="<a id=lienRes href= $urlRes>Réserver cet objet</a></br>";
			}else{
				$res.= "Cet objet a déjà été réservé par quelqu'un.";
			}
			$res = $res . "</section>";
			return $res;
		}

		#Tous les items
		public function affichageItems(){
			$res = "<section> <br/> <table>";
			$res.="<tr>
			<th>Nom</th>
			<th>Description</th>
			<th>Image</th>
			<th>Tarif</th>
			</tr>";
			$app = \Slim\Slim::getInstance();
			$rootUri = $app->request->getRootUri();
			foreach($this->contenu as $k=>$v){
				//$res = $res . var_dump($v) . "<br/>";
				$urlImage = $rootUri."/src/images/".$v["img"];
				$url = $app->urlFor('itemParId',[ 'id' => $v['id']]);
				$res.="<tr>";
				$res.="<td>".$v["nom"]."</td>";
				$res.="<td>".$v["descr"]."</td>";
				$res.="<td> <a id=lienVersItem href=$url><img id=imageItemListe src='$urlImage'></td>";
				$res.="<td>".$v["tarif"]."</td>";
				$res.="</tr>";
			}
			$res = $res . "</section> </table>";

			return $res;
		}

		public function affichageListeUser(){
			$res = "<section> <br/> <table>";
			$res.="<tr>
			<th>Nom</th>
			<th>Url</th>
			</tr>";
			$app = \Slim\Slim::getInstance();
			$listeUrl = $app->urlFor('ajoutItemSansUrl');
			foreach($this->contenu as $k=>$v){
				$urlAjoutItem = $listeUrl."/".$v["url"];
				$url = $v->url;
				$res.="<tr>";
				$res.="<td>".$v->titre."</td>";
				$res.="<td> <a id=listeUrl href=$urlAjoutItem> $url </a></td></br>";
				$res.="</tr>";
			}
			$res = $res . "</section> </table>";
			return $res;
		}

		public function formulaireAjoutItem(){
			$app = \Slim\Slim::getInstance();
    		$listeUrl = $app->urlFor('ajoutItem');
			return "<form method=POST >
			<input type=text name=titre placeholder=Nom de l'item'></input>
			<textarea type=text name=description placeholder=Description de l'item></textarea>
			<input type=text name=prix placeholder=Prix de l'item></input>
			<button type=submit>Ajouter l'item</button>
			<a href=$listeUrl>Retour</a>
			</form>";
		}

		public function itemAjouter(){
			$app = \Slim\Slim::getInstance();
    	$listeUrl = $app->urlFor('ajoutItem');
    	return "<p> Item correctement ajouté a la liste </p>
  		<a href=$listeUrl>Retour</a>";
		}

		public function reserveItem(){
			$nom = $this->contenu['nom'];
			$liste= $this->contenu['liste'];
			$p = $this->contenu['participant'];
			$res = "<section id=reserveItem><p>Vous avez réservé l'objet: $nom,<br> de la liste: $liste.";
			$res.= "<br>Et cela sous le nom de $p.";
			$res.= "<br>Merci à vous.</br></p></section>";
			return $res;
		}

		public function reserveItem_dejaRes(){
			$nom = $this->contenu['nom'];
			$liste= $this->contenu['liste'];
			$res = "<section id=reserveItem><p>Vous avez tenté de réserver l'objet: $nom,<br> de la liste: $liste.";
			$res.= "<br>Malheureusement celui-ci est déjà réservé par quelqu'un...";
			$res.= "<br>Merci à vous.</br></p></section>";
			return $res;
		}

		public function nomParticipant(){
			$app = \Slim\Slim::getInstance();
    	$url = $app->urlFor('reserveItem',['id'=>$this->contenu['id']]);
			$nom = $this->contenu['nom'];
			$liste= $this->contenu['liste'];
			$res = "<section id=nomParticipant><p>Vous souhaitez réserver l'item $nom de la liste $liste.<br>Veuillez entrer votre nom (ou un pseudonyme):<br>";
			$res.= <<<END
			<form method=post action=$url>
			<div class="id"></br>Pseudonyme : <br><input type="text" id="participant" name="participant" placeholder="exemple: Marcel"></br></br></div>
			<button type=submit name="valider_participant" value="participant_nom">valider</button>
			</form>
END;
			return $res;
		}

		#l'affichage général
		private function head(){
			$app = \Slim\Slim::getInstance();
      $rootUri = $app->request->getRootUri();
      $urlCss = $rootUri."/src/css/CssMain.css";
			$head = "<head><meta charset='UTF-8'><title>Wish List</title><link rel=\"stylesheet\" href=\"$urlCss\"></head> ";
			return $head;
		}

		private function foot(){
			$foot = <<<END
      <footer>Made by Ancel Florian, Richard Valentin, Oberlé Loïc, Hof Lucien.</footer>
END;
      return $foot;
		}

		#affichage menu
		private function menu(){
			$app = \Slim\Slim::getInstance();
			$urlAccueil = $app->urlFor('acceuilAff');
			$urlCreateur = $app->urlFor('creerCreateur');
			$urlListes = $app->urlFor('listingListe');
			$urlItems = $app->urlFor('listingItem');
			$urlConnexion = $app->urlFor('connection');
			$urlDeconnexion = $app->urlFor('deconnection');
			$menu = "";
			if($_SESSION['pouvoir'] == 100){
				$menu = <<<END
			<nav class="Menu" style="z-index:3;width:250px" id="Sidebar">
				<a href="$urlAccueil" class='ButtonConnexion'>Accueil</a>
				<a href="$urlCreateur" class='ButtonConnexion'>Créer un compte</a>
				<a href="$urlConnexion" class='ButtonConnexion'>Se connecter</a>
				<a href="$urlListes" class='ButtonMenu'>Afficher listes</a>
				<a href="$urlItems" class='ButtonMenu'>Afficher items</a>
			</nav>
END;
			}else{
				$menu = <<<END
			<nav class="Menu" style="z-index:3;width:250px" id="Sidebar">
				<a href="$urlAccueil" class='ButtonConnexion'>Accueil</a>
				<a href="$urlCreateur" class='ButtonConnexion'>Créer un compte</a>
				<a href="$urlDeconnexion" class='ButtonConnexion'>Deconnexion</a>
				<a href="$urlListes" class='ButtonMenu'>Afficher listes</a>
				<a href="$urlItems" class='ButtonMenu'>Afficher items</a>
			</nav>
END;
			}
			return $menu;
		}

		private function header(){
			return "";
		}

		public function render(){
			$this->afficher($this->select);
		}

		private function afficher($select){
			$contenu="";
			switch($select){
				case VueParticipant::LISTE_LISTES:
					$contenu .= $this->affichageListes();
					break;
				case VueParticipant::LISTE_LISTE_ITEMS:
					$contenu .= $this->affichageListe();
					break;
				case VueParticipant::LISTE_ITEM:
					$contenu .= $this->affichageItem();
					break;
				case VueParticipant::LISTE_ITEMS:
					$contenu .= $this->affichageItems();
					break;
				case VueParticipant::CREATION_ITEM:
					$contenu .= $this->affichageCreationListe();
					break;
				case VueParticipant::LISTE_CREER:
					$contenu .= $this->listeCreer();
					break;
				case VueParticipant::AFFICHAGE_LISTE_USER:
					$contenu .= $this->affichageListeUser();
					break;
				case VueParticipant::FORMULAIRE_AJOUT_ITEM:
					$contenu .= $this->formulaireAjoutItem();
					break;
				case VueParticipant::ITEM_AJOUTER:
					$contenu .= $this->itemAjouter();
					break;
				case VueParticipant::RESERVE_ITEM:
					$contenu.= $this->reserveItem();
					break;
				case VueParticipant::RESERVE_ITEM_deja_res:
					$contenu.=$this->reserveItem_dejaRes();
					break;
				case VueParticipant::RESERVE_ITEM_NomParticipant:
					$contenu.= $this->nomParticipant();
					break;
			}

			$head = $this->head();
			$foot = $this->foot();
			$menu = $this->menu();
			$header = $this->header();
			$html = <<<END
			<!DOCTYPE html>
			<html lang="fr">
				$head
				<body>
					$menu
					<header>$header</header>
					<div class="contenu">$contenu</div>
						$foot
				</body>
			</html>
END;
			echo $html;
		}
	}
