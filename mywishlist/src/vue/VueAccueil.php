<?php
	namespace mywishlist\vue;

	class VueAccueil{

		private $value;
		public function __construct($v){
			$this->value = $v;
		}

		# head, css
		public static function head(){
      $app = \Slim\Slim::getInstance();
      $rootUri = $app->request->getRootUri();
      $urlCss = $rootUri."/src/css/CssMain.css";
			$head = "<head><meta charset='UTF-8'><title>Wish List</title><link rel=\"stylesheet\" href=\"$urlCss\"></head> ";
			return $head;
		}

		# affichage footer
    public static function foot(){
      $foot = <<<END
      <footer>Made by Ancel Florian, Richard Valentin, Oberlé Loïc, Hof Lucien.</footer>
END;
      return $foot;
    }

		#affichage menu
		public static function menu(){
			$app = \Slim\Slim::getInstance();
			$urlAccueil = $app->urlFor('acceuilAff');
			$urlCreateur = $app->urlFor('creerCreateur');
			$urlListes = $app->urlFor('listingListe');
			$urlItems = $app->urlFor('listingItem');
			$urlConnexion = $app->urlFor('connection');
			$urlDeconnexion = $app->urlFor('deconnection');
			$menu = "";
			if($_SESSION['pouvoir'] == 100){
							$menu = <<<END
			<nav class="Menu" style="z-index:3;width:250px" id="Sidebar">
				<a href="$urlAccueil" class="ButtonConnexion">Accueil</a>
				<a href="$urlCreateur" class="ButtonConnexion">Créer un compte</a>
				<a href="$urlConnexion" class="ButtonConnexion">Se connecter</a>
				<a href="$urlListes" class="ButtonMenu">Afficher listes</a>
				<a href="$urlItems" class="ButtonMenu">Afficher items</a>
			</nav>
END;
			}else{
			$menu =	<<<END
			<nav class="Menu" style="z-index:3;width:250px" id="Sidebar">
				<a href="$urlAccueil" class="ButtonConnexion">Accueil</a>
				<a href="$urlCreateur" class="ButtonConnexion">Créer un compte</a>
				<a href="$urlDeconnexion" class="ButtonConnexion">Deconnexion</a>
				<a href="$urlListes" class="ButtonMenu">Afficher listes</a>
				<a href="$urlItems" class="ButtonMenu">Afficher items</a>
			</nav>
END;
			}
			return $menu;
		}

		public static function contenu(){
			$app = \Slim\Slim::getInstance();
			$url = $app->urlFor('fonctionnalites');
			return <<<END
			Voici notre application WishList,<br>
			réalisée par:<br>
			ANCEL Florian,<br>
			RICHARD Valentin,<br>
			HOF Lucien,<br>
			OBERLÉ Loïc.<br><br>
		  Vous pouvez savoir qui a réalisé quoi plus en détail en cliquant <a href = $url>ici</a>.
END;
		}

		public static function fonctionnalites(){
			$res = <<<END
			<section id=fonctionnalites>

			<h1>HOF Lucien</h1>
			<p><ul><li>génération d'url aleatoire</li></ul></p>

			<h1>RICHARD Valentin</h1>
			<p><ul><li>compte createur</li>
			<li>ajout d'item</li>
			<li>partage par url</li>
			</ul></p>

			<h1>OBERLÉ Loïc</h1>
			<p><ul><li>création de liste</li>
			<li>correction de bugs divers</li>
			<li>mise en place du site sur Webetu</li>
			</ul></p>

			<h1>ANCEL Florian</h1>
			<p><ul><li>reservation</li>
			<li>css du site</li></ul></p>
END;
			return $res;
		}

    public function render(){
			switch ($this->value) {
				case 1:
					$contenu = VueAccueil::contenu();
					break;
				case 2:
					$contenu = VueAccueil::fonctionnalites();
					break;
			}

			$head = VueAccueil::head();
			$menu = VueAccueil::menu();
			$foot = VueAccueil::foot();

			$html = <<<END
			<!DOCTYPE html>
			<html lang="fr">
				$head
				<body>
					$menu
					<div class="contenu">$contenu</div>
						$foot
				</body>
			</html>
END;
			echo $html;
    }

  }
