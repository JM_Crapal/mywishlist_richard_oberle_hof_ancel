<?php
	namespace mywishlist\controleur;

	use mywishlist\models\Item as item;
	use mywishlist\models\Liste as liste;
	use mywishlist\vue\VueCreateur as vueCreateur;
	use mywishlist\vue\VueCreationCompte as vueCreation;

	class controleurCreateur {

		public function creerCompte(){
			//1 est l'appel a la fonction d'affichage de l'interface creation compte
			$v = new vueCreation(1);
			$v->render();
			exit();
		}

		public function controleConnection(){
			//2 est l'appel a la fonction d'affichage de l'interface connection
			$v = new vueCreation(2);
			$v->render();
			exit();
		}
	}