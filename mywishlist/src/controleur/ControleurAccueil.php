<?php
namespace mywishlist\controleur;
use mywishlist\vue\VueAccueil;
class ControleurAccueil {

  public function accueilAffiche(){
    $v = new VueAccueil(1);
    $v->render();
  }

  public function fonctionnalites(){
    $v = new VueAccueil(2);
    $v->render();
  }
}
