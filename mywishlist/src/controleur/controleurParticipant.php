<?php
	namespace mywishlist\controleur;

	use mywishlist\models\Item as item;
	use mywishlist\models\Liste as liste;
	use mywishlist\vue\VueParticipant as vueParticipant;
	use mywishlist\models\Utilisateur as utilisateur;
	use mywishlist\models\Reservation;

class controleurParticipant {

	public function __construct(){
		//pas de code comme ca
	}

	public function getItemById( $id ){
		$itemId = item::where('id','=',$id)->first();
		$tabItem=array("nom"=>$itemId->nom,"descr"=>$itemId->descr,"img"=>$itemId->img,"tarif"=>$itemId->tarif,"id"=>$id);
		//on va voir si l'item est reserve
		$r = Reservation::where("item_id",'=',"$id")->first();
		if(isset($r)){
			$tabItem['reserve']="true";
		}else{
			$tabItem['reserve']="false";
		}
		$v = new vueParticipant( $tabItem ,  3);
		$v->render();
	}


	public function getAllListe(){
		$listes = liste::get();
		foreach ($listes as $key => $value) {
			$tabListes[]=array("no"=>$value->no,"user_id"=>$value->user_id,"titre"=>$value->titre,"description"=>$value->description,"expiration"=>$value->expiration,"token"=>$value->token);
		}
		//echo var_dump($tabListes);
		$v = new vueParticipant( $tabListes, 1);
		$v->render();
	}

	////////////////////////////////////////////////////////////////
	////////// ATTENTION, a ma dernire review de ce code, cette fonction ne
	///////// correspondait a aucun chemin dans le index.php
	//////// cette fonction semble donc ne pas devoir exister
	/////// qu'en pensez-vous? Flo ////////12/12/2017/////////
	public function getItemByListe( $idliste ){
		$items = item::where('liste_id','=',$idliste)->get();
		//on va chercher, pour chaque item, s'il a été réservé
		foreach ($items as $key => $i) {
			# code...
		}


		$v = new vueParticipant( $items,2 );
		$v->render();
	}

	public function getListeItems(){
		$contenu = item::get();
		foreach ($contenu as $key => $value) {
			$value=array("nom"=>$value->nom,"descr"=>$value->descr,"img"=>$value->img,"tarif"=>$value->tarif);
		}
		$v = new vueParticipant( $contenu ,4);
		$v->render();
	}

	public function getListeById($idliste){

		$listeItem=item::where('liste_id','=',$idliste)->get();

		foreach ($listeItem as $key => $value) {

			$value=array("id"=>$value->id,"nom"=>$value->nom,"descr"=>$value->descr,"img"=>$value->img,"tarif"=>$value->tarif);
		}
		$v = new vueParticipant( $listeItem,2 );
		$v->render();
	}

	public function creerListe(){
		if(isset($_POST['titre']) and isset($_POST['description']) and isset($_POST['dateExpiration'])){
			$l=new Liste();
			$l->titre=$_POST['titre'];
			$l->description=$_POST['description'];
			$date = date('Y-m-d', strtotime($_POST['dateExpiration']));
			$l->expiration=$date;
			$identifiant = $_SESSION['identifiant'];
			$user = utilisateur::select('idUser')->where('identifiant','=',"$identifiant")->first();
			$l->user_id= $user->idUser;
			$app = \Slim\Slim::getInstance();
			$urlConfirmation = $app->urlFor('listeParUrlSansUrl');
			$urlBase = str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
			$l->url=$urlBase;
			$_SESSION['erreur'] = $urlBase;

			$l->save();
			$v=new VueParticipant("",6);
			$v->render();

		}else{
			$v=new VueParticipant("",5);
			$v->render();
		}
	}

	public function affichageListeByUrl($url){
		if($liste = liste::select('titre','description','expiration')->where('url','like',"$url")->first()){
			$v=new VueParticipant($liste,2);
			$v->render();
		}else{
			echo $liste;
			echo "</br>Cette liste n'existe pas";
		}
	}

	public function afficherListeUtilisateur(){
		$identifiant = $_SESSION['identifiant'];
		$userId = utilisateur::select('idUser')->where('identifiant','like',"$identifiant")->first();
		$user_id = $userId->idUser;
		if($liste = liste::select('titre','url')->where('user_id','=',"$user_id")->get()){
			$v = new VueParticipant($liste,7);
			$v->render();
		}else{
			echo "</br>Vous n'avez aucune liste de creer";
		}
	}

	public function preparationItemDansListe($url){
		$_SESSION['url'] = $url;
		$v=new VueParticipant($url,8);
		$v->render();
	}

	public function ajoutItemDansListe($url){
		$app = \Slim\Slim::getInstance();
		if(isset($_POST['titre']) and isset($_POST['description']) and isset($_POST['prix'])){
			if($liste = liste::select('titre','description','expiration','no')->where('url','like',"$url")->first()){
				$i=new item();
				$i->nom=$_POST['titre'];
				$i->descr=$_POST['description'];
				$i->tarif=$_POST['prix'];
				$i->liste_id= $liste->no;
				$urlConfirmation = $app->urlFor('ajoutItem');
				$i->save();
				$v=new VueParticipant("",9);
				$v->render();
			}
		}else{
			$_SESSION['erreur'] = "Vous n'avez pas renseigner les informations suffisante a la création d'un item";
			$urlConfirmation = $app->urlFor('ajoutItemSansUrl');
			$urlConfirmation .= "/".$_SESSION['url'];
			header("Location: $urlConfirmation");
			exit();
		}
		exit();
	}

	public function reserveItem($id){
		$app = \Slim\Slim::getInstance();
		//on verif que l'item n'est pas deja reserve
		$r = Reservation::where('item_id','=',"$id")->first();

		$item = Item::select('nom','liste_id')->where('id','=',"$id")->first();
		$i = $item['liste_id'];
		$liste = liste::select('user_id','titre')->where('no','=',"$i")->first();
		if(!isset($r['item_id'])){
			$v = new VueParticipant(['nom'=>$item['nom'] , 'liste'=>$liste['titre'] , 'participant'=>$_POST['participant']],10);
			//reservation dans la bdd
			$r = new Reservation();
			$r->item_id=$id;
			$r->save();
			$v->render();
		}else{
			$v = new VueParticipant(['nom'=>$item['nom'] , 'liste'=>$liste['titre']],11);
			$v->render();
		}
	}

	public function nomParticipant($id_item){
		$r = Reservation::where('item_id','=',"$id_item")->first();

		$item = Item::select('nom','liste_id')->where('id','=',"$id_item")->first();
		$i = $item['liste_id'];
		$liste = liste::select('user_id','titre')->where('no','=',"$i")->first();
		if(!isset($r['item_id'])){
			$v = new VueParticipant(['nom'=>$item['nom'] , 'liste'=>$liste['titre'], 'id'=>$id_item] , 12);
			$v->render();
		}else{
			$v = new VueParticipant(['nom'=>$item['nom'] , 'liste'=>$liste['titre'] , 'participant'=>$r['nomParticipant']],11);
			$v->render();
		}
	}

}
