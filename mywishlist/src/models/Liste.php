<?php
	namespace mywishlist\models;

	class Liste extends \illuminate\database\Eloquent\Model {
		protected $table = 'liste';
		protected $primaryKey = 'no';
		public $timestamps = false;

		public function items(){
			return $this->hasMany('mywishlist\models\Item','no');
		}
		public function reservations(){
			return $this->hasMany('mywishlist\models\Reservation','no');
		}
	}
