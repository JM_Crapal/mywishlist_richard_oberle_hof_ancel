<?php
	namespace mywishlist\models;

	class Utilisateur  extends \Illuminate\Database\Eloquent\Model{
		protected $table = 'utilisateur';
		protected $primaryKey = 'identifiant';
		public $timestamps = false;

	}