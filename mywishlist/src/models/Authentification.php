<?php
	namespace mywishlist\models;
	use mywishlist\models\Utilisateur as utilisateur;

	class Authentification {

		public static function createUser($id,$mdp,$confirmMdp){
			$_SESSION['erreur'] = "";
			$testErreur = 0;
			if($mdp != $confirmMdp){
				$_SESSION['erreur'] = $_SESSION['erreur']."Les mot de passes ne sont pas les mêmes </br>";
				$testErreur = 1;
			}
			if(strlen($id)>32){
				$_SESSION['erreur'] = $_SESSION['erreur']."L'identifiant depasse 32 caractères </br>";
				$testErreur = 1;
			}
			if($id !== filter_var($id,FILTER_SANITIZE_STRING)){
				$_SESSION['erreur'] = $_SESSION['erreur']."L'identifiant utilise des caractères spéciaux </br>";
				$testErreur = 1;
			}
			if($mdp !== filter_var($mdp,FILTER_SANITIZE_STRING)){
				$_SESSION['erreur'] = $_SESSION['erreur']."Le mot de passe utilise des caractères spéciaux </br>";
				$testErreur = 1;
			}
			if($testIdUnique = utilisateur::select('identifiant')->where('identifiant','=',"$id")->first()){
				$_SESSION['erreur'] = $_SESSION['erreur']."L'identifiant est indisponible </br>";
			}
			if($testErreur === 1){
				$app = \Slim\Slim::getInstance();
				$urlConfirmation = $app->urlFor('creerCreateur');
				header("Location:  $urlConfirmation");
				exit();
			}else{

				$vraiMdp = hash('sha256',$mdp);
				$newUser = new Utilisateur();
				$newUser->identifiant = $id;
				$newUser->mdp = $vraiMdp;
				$newUser->pouvoir = 110;
				$newUser->save();
				$app = \Slim\Slim::getInstance();
				$urlConfirmation = $app->urlFor('compteCreer');
				echo $urlConfirmation;
				header("Location:  $urlConfirmation");
				exit();
			}
		}


		public static function authenticate ($id , $password){
			$app = \Slim\Slim::getInstance();
			$urlConfirmation = $app->urlFor('connection');
			$eviteInjection = 1;
			if($id !== filter_var($id,FILTER_SANITIZE_SPECIAL_CHARS)){
				$eviteInjection = 0;
			}
			if($password !== filter_var($password,FILTER_SANITIZE_SPECIAL_CHARS)){
				$eviteInjection = 0;
			}
			if($eviteInjection == 1){
				if($authentification = utilisateur::select('mdp','pouvoir')->where('identifiant','=',"$id")->first()){
					$testMdp = hash('sha256',$password);
					if($authentification->mdp === $testMdp){
						$_SESSION['identifiant'] = $id;
						$_SESSION['pouvoir'] = $authentification->pouvoir;
						$urlConnecte = $app->urlFor('connecter');
						echo ($urlConnecte);
						header("Location: $urlConnecte");
						exit();
					}else{
						echo "$urlConfirmation";
						$_SESSION['erreur'] = "Votre mot de passe ou identifiant est faux </br> Veuillez Reessayer";
						header("Location: $urlConfirmation");
						exit();
					}
				}else{
					$_SESSION['erreur'] = "Votre mot de passe ou identifiant est faux </br> Veuillez Reessayer";
					header("Location: $urlConfirmation");
					exit();
				}
			}else{
				$_SESSION['erreur'] = "Si vous pouviez eviter les injection sql ce serait sympa";
				header("Location: $urlConfirmation");
				exit();
			}
		}	
	}