<?php
	require 'vendor/autoload.php';
	use Illuminate\Database\Capsule\Manager as DB;
	use mywishlist\models\Item as item;
	use mywishlist\models\Liste as liste;
	use mywishlist\controleur\controleurCreateur as cCreateur;
	use mywishlist\controleur\controleurParticipant as cParticipant;
  use mywishlist\controleur\ControleurAccueil;
	use mywishlist\models\Utilisateur as utilisateur;
	use mywishlist\models\Authentification as authentification;

	$db = new DB();
	$conf=parse_ini_file('src/conf/conf.ini');
	$db->addConnection($conf);
	$db->setAsGlobal();
	$db->bootEloquent();


	session_start();
	if(isset($_SESSION['pouvoir'])){

	}else{
		$_SESSION['pouvoir'] = 100;
	}

	if(isset($_SESSION['pouvoir'])){

	}else{
		$_SESSION['identifiant'] = "utilisateur";
	}

	$app = new \Slim\Slim();

	$app = \Slim\Slim::getInstance();
	$rootUri = $app->request->getRootUri();
	$urlCss = $rootUri."/src/css/CssMain.css";
	$head = "<head><title>Wish List</title><link rel=\"stylesheet\" type=\"text/css\" href=\"$urlCss\"/></head> ";
	//echo "$head";

	$app->get('/liste/items',function(){
		$controlParti = new cParticipant();
		$controlParti->getListeItems();
	})->name('listingItem');

	$app->get('/',function(){
		header('Location: accueil');
		exit();
	});

	$app->get('/accueil',function(){
		$c = new ControleurAccueil();
    $c->accueilAffiche();
	})->name('acceuilAff');

	$app->get('/liste/listes',function(){
		$controlParti = new cParticipant();
		$controlParti->getAllListe();
	})->name('listingListe');

	$app->get('/liste/itemId/:id',function($id){
		$controlParti = new cParticipant();
		$controlParti->getItemById($id);
	})->name('itemParId');

	//Permet d'afficher le contenu d'une liste preciser par son id
	$app->get('/liste/listeId/:id',function($id){
		$controlParti = new cParticipant();
		$controlParti->getListeById($id);
	})->name('listeParId');


	$app->get('/liste/creer',function(){
		if($_SESSION['pouvoir']>100){
			$control = new cParticipant();
			$control->creerListe();
		}else{
			echo "Vous devez etre connecté en tant que créateur pour créer une liste";
		}
	})->name('creerListeGet');

	$app->post('/liste/creer',function(){
		$control = new cParticipant();
		$control->creerListe();
	})->name('creerListePost');

	$app->get('/creer/createur',function(){
		$control = new cCreateur();
		$control->creerCompte();
		exit();
	})->name('creerCreateur');

	$app->post('/creer/createur/confirmationCompte',function(){
		$id = $_POST["identifiant"];
		$mdp = $_POST["motDePasse"];
		$confirMdp = $_POST["confirmeMdp"];
		authentification::createUser($id,$mdp,$confirMdp);
	})->name('confirmerCreateur');

	$app->get('/creer/compteCreer',function(){
		header('Location: ../accueil');
		exit();
	})->name('compteCreer');

	$app->get('/connection',function(){
		if($_SESSION['pouvoir']>100){
			echo "Vous etes déja connecté";
		}else{
			$controlConnection = new cCreateur();
			$controlConnection->controleConnection();
			exit();//////////////////////////ICI
		}
		exit();
	})->name('connection');

	$app->post('/connection/confirmation',function(){
		$id = $_POST['identifiant'];
		$mdp = $_POST['motDePasse'];
		authentification::authenticate($id,$mdp);
	})->name('confirmerConnection');

	$app->get('/connecter',function(){
		$app = \Slim\Slim::getInstance();
		echo "Vous etes connecte en tant que ".$_SESSION['identifiant'];
		header("Location:".$app->urlFor('acceuilAff'));
		exit();
	})->name('connecter');

	$app->get('/deconnection',function(){
		$_SESSION['identifiant'] = "utilisateur";
		$_SESSION['pouvoir'] = 100;
		echo "Vous etes deconnecter";
		$app = \Slim\Slim::getInstance();
header("Location:".$app->urlFor('acceuilAff'));
		exit();
	})->name('deconnection');

	$app->get('/liste/url/:url',function($url){
		$controlListe = new cParticipant();
		$controlListe->affichageListeByUrl($url);
	})->name('listeParUrl');

	$app->get('/liste',function($url){
	})->name('listeParUrlSansUrl');

	$app->get('/liste/addItem',function(){
		if($_SESSION['pouvoir']>100){
			$control = new cParticipant();
			$control->afficherListeUtilisateur();
		}else{
			echo "Vous devez etre connecté en tant que créateur pour créer une liste";
		}
	})->name('ajoutItem');

	$app->get('/liste/addItemUrl/:url',function($url){
		$control = new cParticipant();
		$control->preparationItemDansListe($url);
	})->name('preparationItemDansListe');

	$app->get('/liste/addItemUrl',function(){
	})->name('ajoutItemSansUrl');

	$app->post('/res/item/:id',function($id){
		$c = new cParticipant();
		$c->reserveItem($id);
	})->name('reserveItem');

	$app->get('/resNom/item/:id', function($id){
		$c = new cParticipant();
		$c->nomParticipant($id);
	})->name('participantRes');

	$app->post('/liste/addItemUrl/:url',function($url){
		$control = new cParticipant();
		$control->ajoutItemDansListe($url);
	})->name('ajoutItemDansListe');

	$app->get('/accueil/fonctionnalites',function(){
		$c = new ControleurAccueil();
		$c->fonctionnalites();
	})->name('fonctionnalites');
	$app->run();
